# Dax Blank
The objective of this repo is to show my PHP, Wordpress and Jquery programming abilities. I use this theme to develop websites for my clients.

***

*Dax Blank* is a blank Wordpress starter theme for developers. It is mean to act as a backend framework only loading mainly the functions and variables, the assets and styles should be loaded in the child theme. The reason behind this approach is to:
* Speed up development time.
* Separate functionalities and look.
* Avoid conflicts when updating functionalities.
